#!/usr/bin/env python3
"""
Convert.py takes exactly 3 arguments - Input energy, Input units, Output units

Valid Units:

Hartree (hartree, Eh)
eV      (ev)
Joules  (J)
kJmol
kcal
nm

@author: Stephen Mason
"""

import sys
from decimal import Decimal as D
from scipy import constants as con


class Convert:
    """A class to convert bewtween various units of energy"""

    # Conversion factors to Joules
    Joules = 1
    Hartree = (con.m_e * con.e**4/(4 * con.pi * con.epsilon_0 * con.hbar)**2)
    kJmol = 1000 / con.N_A
    kcal = 4184 / con.N_A
    eV = con.e
    nm = (con.h * con.c) / (10**-9)

    def __init__(self, energy, in_units, out_units):
        self.energy = energy
        self.in_units = in_units
        self.out_units = out_units

    @classmethod
    def factor(cls, units):
        """Contains the dictionary of energy units availabel to translate"""
        funcs = {
            'Eh': Convert.Hartree,
            'Hartree': Convert.Hartree,
            'hartree': Convert.Hartree,
            'eV': Convert.eV,
            'ev': Convert.eV,
            'J': Convert.Joules,
            'Joules': Convert.Joules,
            'kJmol': Convert.kJmol,
            'kcal': Convert.kcal,
            'nm': Convert.nm,
        }
        factor = funcs[units]
        return factor

    def input_to_joules(self):
        """A function to convert user input to Joules"""
        if self.in_units == 'nm':
            joules = self.factor(self.in_units) / self.energy
        else:
            joules = self.factor(self.in_units) * self.energy
        return joules

    def output(self):
        """A function to convert Joules to user-defined units"""
        joules = self.input_to_joules()
        if self.out_units == 'nm':
            output = self.factor(self.out_units) / joules
        else:
            output = joules / self.factor(self.out_units)
        return output, self.out_units


def main():
    """This function is only called if convert.py is being ran interactively"""
    try:
        query = Convert(float(sys.argv[1]), str(sys.argv[2]), str(sys.argv[3]))
        e_in, units_in = query.energy, query.in_units
        e_out, units_out = query.output()
        print('{} {} = {:.3E} {}'.format(e_in, units_in, D(e_out), units_out))
    except IndexError:
        description = __doc__
        print(description)
        exit()


if __name__ == "__main__":
    main()
